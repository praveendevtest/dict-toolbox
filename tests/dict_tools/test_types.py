from typing import List

from dict_tools.typing import Computed


def test_computed_type():
    def _() -> Computed[str]:
        ...

    def _() -> Computed[List[str]]:
        ...
